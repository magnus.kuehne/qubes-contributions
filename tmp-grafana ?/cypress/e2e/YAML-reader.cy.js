describe("YAML reader", () => {
  it("reads content of a YAML config file", () => {
    const YAML = require('yamljs')
    try {
      const qube = YAML.parse(cy.readFile('qube.yaml', 'utf8'));
      console.log(qube);
      const url = qube.grafana.metadata.ingress
      const username = qube.grafana.properties.username
      const password = qube.grafana.properties.password

      cy.visit(url)
      cy.get("#username").type(username)
      cy.get("#password").type(password)
      cy.get("#someSubmitButton").click()
    } catch (e) {
      console.log(e);
    }
  })
})
