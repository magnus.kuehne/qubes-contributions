qubeKey: source-teradata
name: source-teradata
type: airbyte-source
id: source-teradata
maturity: alpha
ui:
  icon: <svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 48 48" width="48px"
    height="48px"><path fill="#f27241" d="M24 4A20 20 0 1 0 24 44A20 20 0 1 0 24 4Z"/><path
    fill="#fff" d="M30.515,31.366c-1.446,0.609-3.478,1.137-4.935,0.192c-1.159-0.752-1.58-2.116-1.58-2.85V21h5v-5h-5
    v-5h-5v17.707c0,2.064,1.002,5.124,3.828,6.956c1.08,0.7,2.623,1.336,4.665,1.336c1.419,0,3.08-0.306,4.993-1.113L30.515,31.366z"/></svg>
  shortDescription: database
  description: ''
  links:
  - linkName: Documentation
    url: https://docs.airbyte.com/integrations/sources/teradata
properties:
  type: object
  title: Teradata Source Spec
  schema: http://json-schema.org/draft-07/schema#
  required:
  - host
  - database
  - username
  properties:
    ssl:
      type: boolean
      order: 7
      title: SSL Connection
      default: false
      description: Encrypt data using SSL. When activating SSL, please select one
        of the connection modes.
    host:
      type: string
      order: 0
      title: Host
      description: Hostname of the database.
    port:
      type: integer
      order: 1
      title: Port
      default: 3306
      maximum: 65536
      minimum: 0
      examples:
      - '3306'
      description: Port of the database.
    database:
      type: string
      order: 2
      title: Database
      description: Name of the database.
    password:
      type: string
      order: 4
      title: Password
      description: Password associated with the username.
      airbyte_secret: true
    ssl_mode:
      type: object
      oneOf:
      - title: disable
        required:
        - mode
        properties:
          mode:
            type: string
            const: disable
            order: 0
        description: Disable SSL.
        additionalProperties: true
      - title: allow
        required:
        - mode
        properties:
          mode:
            type: string
            const: allow
            order: 0
        description: Allow SSL mode.
        additionalProperties: true
      - title: prefer
        required:
        - mode
        properties:
          mode:
            type: string
            const: prefer
            order: 0
        description: Prefer SSL mode.
        additionalProperties: true
      - title: require
        required:
        - mode
        properties:
          mode:
            type: string
            const: require
            order: 0
        description: Require SSL mode.
        additionalProperties: true
      - title: verify-ca
        required:
        - mode
        - ssl_ca_certificate
        properties:
          mode:
            type: string
            const: verify-ca
            order: 0
          ssl_ca_certificate:
            type: string
            order: 1
            title: CA certificate
            multiline: true
            description: "Specifies the file name of a PEM file that contains Certificate\
              \ Authority (CA) certificates for use with SSLMODE=verify-ca.\n See\
              \ more information - <a href=\"https://teradata-docs.s3.amazonaws.com/doc/connectivity/jdbc/reference/current/jdbcug_chapter_2.html#URL_SSLCA\"\
              > in the docs</a>."
            airbyte_secret: true
        description: Verify-ca SSL mode.
        additionalProperties: true
      - title: verify-full
        required:
        - mode
        - ssl_ca_certificate
        properties:
          mode:
            type: string
            const: verify-full
            order: 0
          ssl_ca_certificate:
            type: string
            order: 1
            title: CA certificate
            multiline: true
            description: "Specifies the file name of a PEM file that contains Certificate\
              \ Authority (CA) certificates for use with SSLMODE=verify-full.\n See\
              \ more information - <a href=\"https://teradata-docs.s3.amazonaws.com/doc/connectivity/jdbc/reference/current/jdbcug_chapter_2.html#URL_SSLCA\"\
              > in the docs</a>."
            airbyte_secret: true
        description: Verify-full SSL mode.
        additionalProperties: true
      order: 8
      title: SSL Modes
      description: "SSL connection modes. \n <b>disable</b> - Chose this mode to disable\
        \ encryption of communication between Airbyte and destination database\n <b>allow</b>\
        \ - Chose this mode to enable encryption only when required by the destination\
        \ database\n <b>prefer</b> - Chose this mode to allow unencrypted connection\
        \ only if the destination database does not support encryption\n <b>require</b>\
        \ - Chose this mode to always require encryption. If the destination database\
        \ server does not support encryption, connection will fail\n  <b>verify-ca</b>\
        \ - Chose this mode to always require encryption and to verify that the destination\
        \ database server has a valid SSL certificate\n  <b>verify-full</b> - This\
        \ is the most secure mode. Chose this mode to always require encryption and\
        \ to verify the identity of the destination database server\n See more information\
        \ - <a href=\"https://teradata-docs.s3.amazonaws.com/doc/connectivity/jdbc/reference/current/jdbcug_chapter_2.html#URL_SSLMODE\"\
        > in the docs</a>."
    username:
      type: string
      order: 3
      title: Username
      description: Username to use to access the database.
    jdbc_url_params:
      type: string
      order: 5
      title: JDBC URL params
      description: 'Additional properties to pass to the JDBC URL string when connecting
        to the database formatted as ''key=value'' pairs separated by the symbol ''&''.
        (example: key1=value1&key2=value2&key3=value3)'
    replication_method:
      enum:
      - STANDARD
      - CDC
      type: string
      order: 6
      title: Replication method
      default: STANDARD
      description: Replication method to use for extracting data from the database.
        STANDARD replication requires no setup on the DB side but will not be able
        to represent deletions incrementally. CDC uses the Binlog to detect inserts,
        updates, and deletes. This needs to be configured on the source database itself.
schemaType: {}
