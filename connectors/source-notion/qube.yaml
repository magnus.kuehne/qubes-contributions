qubeKey: source-notion
name: source-notion
type: airbyte-source
id: source-notion
maturity: generally_available
ui:
  icon: <svg xmlns="http://www.w3.org/2000/svg" width="250" height="250" fill="none"><path
    fill="#000" fill-rule="evenodd" d="M64.352 62.525c6.003 4.877 8.255 4.505 19.528
    3.752l106.271-6.38c2.254 0 .379-2.25-.372-2.623l-17.65-12.759c-3.381-2.626-7.887-5.632-16.522-4.88L52.704
    47.14c-3.752.372-4.502 2.249-3.008 3.753l14.656 11.632Zm6.38 24.766v111.816c0
    6.009 3.004 8.257 9.762 7.886l116.792-6.758c6.763-.372 7.516-4.506 7.516-9.387V79.782c0-4.873-1.875-7.502-6.014-7.126l-122.05
    7.126c-4.504.379-6.006 2.632-6.006 7.51ZM186.03 93.29c.749 3.38 0 6.758-3.387
    7.138l-5.627 1.121v82.55c-4.885 2.626-9.391 4.127-13.145 4.127-6.011 0-7.517-1.878-12.019-7.503l-36.809-57.785v55.909l11.648
    2.628s0 6.751-9.397 6.751l-25.907 1.503c-.752-1.503 0-5.252 2.628-6.003l6.76-1.874V107.93l-9.386-.752c-.753-3.381
    1.122-8.255 6.383-8.633l27.792-1.873 38.307 58.537v-51.784l-9.767-1.121c-.75-4.132
    2.25-7.133 6.006-7.505l25.92-1.509ZM44.065 37.01 151.1 29.128c13.144-1.127 16.526-.372
    24.787 5.63l34.167 24.014c5.638 4.13 7.517 5.253 7.517 9.755v131.709c0 8.254-3.008
    13.136-13.52 13.883l-124.3 7.506c-7.892.376-11.648-.749-15.781-6.005l-25.161-32.645c-4.508-6.01-6.383-10.505-6.383-15.765V50.138c0-6.75
    3.008-12.381 11.639-13.128Z" clip-rule="evenodd"/></svg>
  shortDescription: api
  description: ''
  links:
  - linkName: Documentation
    url: https://docs.airbyte.com/integrations/sources/notion
properties:
  type: object
  title: Notion Source Spec
  schema: http://json-schema.org/draft-07/schema#
  required:
  - credentials
  properties:
    start_date:
      type: string
      title: Start Date
      format: date-time
      pattern: ^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}.[0-9]{3}Z$
      examples:
      - '2020-11-16T00:00:00.000Z'
      description: UTC date and time in the format YYYY-MM-DDTHH:MM:SS.000Z. During
        incremental sync, any data generated before this date will not be replicated.
        If left blank, the start date will be set to 2 years before the present date.
      pattern_descriptor: YYYY-MM-DDTHH:MM:SS.000Z
    credentials:
      type: object
      oneOf:
      - type: object
        title: OAuth2.0
        required:
        - auth_type
        - client_id
        - client_secret
        - access_token
        properties:
          auth_type:
            type: string
            const: OAuth2.0
          client_id:
            type: string
            title: Client ID
            description: The Client ID of your Notion integration. See our <a href='https://docs.airbyte.com/integrations/sources/notion#step-2-set-permissions-and-acquire-authorization-credentials'>docs</a>
              for more information.
            airbyte_secret: true
          access_token:
            type: string
            title: Access Token
            description: The Access Token received by completing the OAuth flow for
              your Notion integration. See our <a href='https://docs.airbyte.com/integrations/sources/notion#step-2-set-permissions-and-acquire-authorization-credentials'>docs</a>
              for more information.
            airbyte_secret: true
          client_secret:
            type: string
            title: Client Secret
            description: The Client Secret of your Notion integration. See our <a
              href='https://docs.airbyte.com/integrations/sources/notion#step-2-set-permissions-and-acquire-authorization-credentials'>docs</a>
              for more information.
            airbyte_secret: true
      - type: object
        title: Access Token
        required:
        - auth_type
        - token
        properties:
          token:
            type: string
            title: Access Token
            description: The Access Token for your private Notion integration. See
              the <a href='https://docs.airbyte.com/integrations/sources/notion#step-1-create-an-integration-in-notion'>docs</a>
              for more information on how to obtain this token.
            airbyte_secret: true
          auth_type:
            type: string
            const: token
      order: 1
      title: Authentication Method
      description: Choose either OAuth (recommended for Airbyte Cloud) or Access Token.
        See our <a href='https://docs.airbyte.com/integrations/sources/notion#setup-guide'>docs</a>
        for more information.
schemaType: {}
