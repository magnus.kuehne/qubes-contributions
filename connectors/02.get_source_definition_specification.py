### Step
# READ file ./<source-name>/source_definition.json
# POST /source_definition_specifications/get with sourceDefinitionId
# SAVE to corresponding source_definition_specification.json
### Result
# ├── source-github
# │ ├── source_definition.json
# │ └── source_definition_specification.json
# ├── source-monday
#   ├── source_definition.json
#   └── source_definition_specification.json

import os
import json, ast
from decouple import config
import requests

BASEURL = config('BASEURL')
FORMAT_INDENT = config('FORMAT_INDENT', cast=int)
CONNECTOR_DIR = config('CONNECTOR_DIR')

url = f'{BASEURL}/source_definitions/list_latest'

def _get_workspace_id():
    try:
        workspace_url = f'{BASEURL}/workspaces/list'
        print(f'POST {workspace_url}')
        response = requests.post(workspace_url)
        data = response.json()
        workspace_id = data['workspaces'][0]['workspaceId']
        print(f'workspace_id: {workspace_id}')
        return workspace_id
    except Exception as e:
        print(f'ERROR: {e}')
    
def get_source_definition_specification() -> None:
    # get workspace_id
    workspace_id = _get_workspace_id()
    
    for qube_key in next(os.walk(CONNECTOR_DIR))[1]:
        source_dir = f'{CONNECTOR_DIR}/{qube_key}'
        
        # process if directory start with 'source-'
        if qube_key.startswith('source-'):
            # source_dir = ./source-github
            # source_dir_name = <connector-dir>/source-github
            print(f'Processing {qube_key}')
            try:
                # read sourceDefinitionId in each dir
                with open(f'{source_dir}/source_definition.json', 'r') as file:
                    contents = json.load(file)
                    source_definition_id = contents['sourceDefinitionId']
                    
                    # call to source_definition_specifications
                    source_definition_url = f'{BASEURL}/source_definition_specifications/get'
                    header = {
                        'Content-Type': 'application/json'
                    }
                    payload = {
                        'workspaceId': workspace_id,
                        'sourceDefinitionId': source_definition_id
                    }
                    response = requests.post(source_definition_url, headers=header, json=payload)
                    data = response.json()
                    
                    # save content to source_definition_specification.json
                    print(f'Saving data to {source_dir}/source_definition_specification.json')
                    with open(f'{source_dir}/source_definition_specification.json', 'w') as file:
                        json.dump(data, file, indent=FORMAT_INDENT)
            except Exception as e:
                print(f'ERROR-{qube_key}: {e}')

if __name__ == '__main__':
    get_source_definition_specification()


