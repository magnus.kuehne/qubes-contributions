# FINAL CRDs
import os
import json
from decouple import config
import yaml

FORMAT_INDENT = config('FORMAT_INDENT', cast=int)
CONNECTOR_DIR = config('CONNECTOR_DIR')
CRD_SOURCE_FILE = config('CRD_SOURCE_FILE')


def get_type(schema_type) -> str:
    """_summary_

    Args:
        schema_type (_type_): A schema.get('type'), either list or str

    Returns:
        str: the type
    """    
    if isinstance(schema_type, list):
        for schema_type in schema_type:
            if schema_type in ('string', 'integer', 'array', 'object'):
                return schema_type
    elif isinstance(schema_type, str):
        return schema_type
    return 'string'


def get_array_item_type(schema: dict) -> str:
    """_summary_

    Args:
        schema (dict): A dictionary of schema with type: array

    Returns:
        str: the type items
    """
    if isinstance(schema.get('item'), str):
        item_type = schema.get('item')
    elif isinstance(schema.get('items'), dict):
        item_type = schema.get('items').get('type', 'string')
    else:  # no item/items
        item_type = 'string'
    return item_type


def convert_json_schema_to_openapiv3(json_schema) -> dict:
    """
    Converts a JSON Schema to an OpenAPI v3 Schema.

    Args:
        json_schema: A dictionary representing the JSON Schema.

    Returns:
        A dictionary representing the OpenAPI v3 Schema.
    """
    openapiv3_schema = {
        'type': 'string',
        'properties': {}
    }

    # handle for type is a list
    if 'type' in json_schema:
        schema_type = get_type(json_schema.get('type'))
        if schema_type in ('string', 'integer', 'boolean', 'object'):
            openapiv3_schema.update({
                'type': schema_type
                })
        elif schema_type in ('array'):
            item_type = get_array_item_type(json_schema)
            openapiv3_schema.update({
                'type': schema_type,
                'items': {
                    'type': item_type
                }
            })

    if 'required' in json_schema:
        openapiv3_schema['required'] = json_schema.get('required')

    if 'enum' in json_schema:
        openapiv3_schema['enum'] = json_schema.get('enum')

    # if key contains oneOf
    if 'oneOf' in json_schema:
        #openapiv3_schema['oneOf'] = []
        for schema in json_schema['oneOf']:

            # process inner oneOf properties:
            for field_name, field_schema in schema.get('properties', {}).items():
                #print(f'field: {field_name} - {field_schema.keys()}')
                openapiv3_schema['properties'][field_name] = convert_json_schema_to_openapiv3(
                    field_schema)

    # process outer properties
    for field_name, field_schema in json_schema.get('properties', {}).items():
        openapiv3_schema['properties'][field_name] = convert_json_schema_to_openapiv3(
                field_schema)

    return openapiv3_schema


def generate_crd() -> None:

    try:
        with open(f'./crd_sources_schema.json', 'r') as file:
            crd_schema = yaml.safe_load(file)

    except Exception as e:
        print(f'ERROR: {e}')

    for qube_key in next(os.walk(CONNECTOR_DIR))[1]:
        if qube_key.startswith('source-') and os.path.isdir(f'{CONNECTOR_DIR}/{qube_key}'):
            print(f'Processing {qube_key}')
            qube_directory = f'{CONNECTOR_DIR}/{qube_key}'
            crd_name = '-'.join(qube_key.split('-')[1:])
            try:
                with open(f'{qube_directory}/source_definition_specification.json', 'r') as file:
                    contents = json.load(file)

                if 'connectionSpecification' in contents.keys():
                    properties_json_schema = contents['connectionSpecification']
                    openapiv3_schema = convert_json_schema_to_openapiv3(
                        properties_json_schema)

                    # set to the current schema
                    crd_schema.get('spec').get('versions')[0].get('schema').get('openAPIV3Schema').get('properties').get(
                        'spec').get('properties').get('source').get('properties').update({crd_name: openapiv3_schema})

            except Exception as e:
                print(f'ERROR-{qube_key}: {e}')

    # print(crd_schema)
    with open(f'{CONNECTOR_DIR}/{CRD_SOURCE_FILE}', 'w') as file:
        yaml.safe_dump(crd_schema, file, default_flow_style=False)


if __name__ == '__main__':
    generate_crd()
