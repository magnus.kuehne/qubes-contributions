qubeKey: source-slack
name: source-slack
type: airbyte-source
id: source-slack
maturity: generally_available
ui:
  icon: <svg xmlns="http://www.w3.org/2000/svg" width="250" height="250" fill="none"><path
    fill="#36C5F0" fill-rule="evenodd" d="M99.52 29.47c-10.561.008-19.109 8.561-19.101
    19.102-.008 10.54 8.547 19.094 19.109 19.102h19.109V48.58c.008-10.54-8.547-19.095-19.117-19.11.008
    0 .008 0 0 0Zm0 50.95H48.578c-10.562.007-19.117 8.56-19.11 19.101-.015 10.541
    8.54 19.095 19.102 19.11h50.95c10.562-.007 19.117-8.561 19.109-19.102.008-10.548-8.547-19.102-19.109-19.11Z"
    clip-rule="evenodd"/><path fill="#2EB67D" fill-rule="evenodd" d="M220.532 99.521c.008-10.54-8.548-19.094-19.109-19.102-10.562.008-19.117
    8.562-19.11 19.102v19.11h19.11c10.561-.007 19.117-8.561 19.109-19.11Zm-50.95 0v-50.95c.008-10.532-8.54-19.086-19.102-19.101-10.561.008-19.117
    8.561-19.109 19.102v50.95c-.015 10.54 8.54 19.094 19.102 19.109 10.561-.007 19.117-8.561
    19.109-19.11Z" clip-rule="evenodd"/><path fill="#ECB22E" fill-rule="evenodd" d="M150.472
    220.53c10.562-.008 19.117-8.562 19.11-19.102.007-10.541-8.548-19.095-19.11-19.103h-19.109v19.103c-.008
    10.532 8.548 19.086 19.109 19.102Zm0-50.958h50.95c10.562-.007 19.117-8.561 19.11-19.102.015-10.54-8.54-19.094-19.102-19.11h-50.95c-10.561.008-19.117
    8.562-19.109 19.103-.008 10.548 8.54 19.102 19.101 19.109Z" clip-rule="evenodd"/><path
    fill="#E01E5A" fill-rule="evenodd" d="M29.469 150.471c-.008 10.541 8.547 19.095
    19.109 19.103 10.561-.008 19.117-8.562 19.11-19.103v-19.102h-19.11c-10.562.008-19.117
    8.562-19.11 19.102Zm50.95 0v50.95c-.016 10.541 8.54 19.094 19.101 19.11 10.562-.008
    19.117-8.562 19.109-19.102v-50.942c.016-10.54-8.539-19.094-19.101-19.11-10.57
    0-19.117 8.554-19.11 19.094 0 .008 0 0 0 0Z" clip-rule="evenodd"/></svg>
  shortDescription: api
  description: ''
  links:
  - linkName: Documentation
    url: https://docs.airbyte.com/integrations/sources/slack
properties:
  type: object
  title: Slack Spec
  schema: http://json-schema.org/draft-07/schema#
  required:
  - start_date
  - lookback_window
  - join_channels
  properties:
    start_date:
      type: string
      title: Start Date
      format: date-time
      pattern: ^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}Z$
      examples:
      - '2017-01-25T00:00:00Z'
      description: UTC date and time in the format 2017-01-25T00:00:00Z. Any data
        before this date will not be replicated.
    credentials:
      type: object
      oneOf:
      - type: object
        order: 0
        title: Sign in via Slack (OAuth)
        required:
        - option_title
        - client_id
        - client_secret
        - access_token
        properties:
          client_id:
            type: string
            title: Client ID
            description: Slack client_id. See our <a href="https://docs.airbyte.com/integrations/sources/slack">docs</a>
              if you need help finding this id.
          access_token:
            type: string
            title: Access token
            description: Slack access_token. See our <a href="https://docs.airbyte.com/integrations/sources/slack">docs</a>
              if you need help generating the token.
            airbyte_secret: true
          option_title:
            type: string
            const: Default OAuth2.0 authorization
          client_secret:
            type: string
            title: Client Secret
            description: Slack client_secret. See our <a href="https://docs.airbyte.com/integrations/sources/slack">docs</a>
              if you need help finding this secret.
            airbyte_secret: true
      - type: object
        order: 1
        title: API Token
        required:
        - option_title
        - api_token
        properties:
          api_token:
            type: string
            title: API Token
            description: A Slack bot token. See the <a href="https://docs.airbyte.com/integrations/sources/slack">docs</a>
              for instructions on how to generate it.
            airbyte_secret: true
          option_title:
            type: string
            const: API Token Credentials
      title: Authentication mechanism
      description: Choose how to authenticate into Slack
    join_channels:
      type: boolean
      title: Join all channels
      default: true
      description: 'Whether to join all channels or to sync data only from channels
        the bot is already in.  If false, you''ll need to manually add the bot to
        all the channels from which you''d like to sync messages. '
    channel_filter:
      type: array
      items:
        type: string
        minLength: 0
      title: Channel name filter
      default: []
      examples:
      - channel_one
      - channel_two
      description: A channel name list (without leading '#' char) which limit the
        channels from which you'd like to sync. Empty list means no filter.
    lookback_window:
      type: integer
      title: Threads Lookback window (Days)
      default: 0
      maximum: 365
      minimum: 0
      examples:
      - 7
      - 14
      description: How far into the past to look for messages in threads, default
        is 0 days
  additionalProperties: true
schemaType: {}
